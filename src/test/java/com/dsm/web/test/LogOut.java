package com.dsm.web.test;

import com.dsm.web.test.utils.BasePage;
import com.dsm.web.test.utils.Path;
import com.thoughtworks.gauge.Step;
import org.openqa.selenium.By;

public class LogOut extends BasePage implements Path {

    @Step("Log out the customer")
    public void logOutTheCustomer() {
        logOut();
    }

    @Step("Clear previous login")
    public void clearPreviousLogin() {
        try {
            clearLogin();
        } catch (Exception ex) {
            System.out.println("no previously logged in Customers");
        }
    }

    private void logOut() {
        driver.findElement(By.xpath(lnk_user)).click();
        waitForXPathElement(lnk_logout);
        driver.findElement(By.xpath(lnk_logout)).click();;
    }

    private void clearLogin() {
        /*
        if(driver.findElement(By.xpath(txtWelcomeUser)) != null) {
            driver.findElement(By.xpath(menu)).click();
            driver.findElement(By.cssSelector(menuLogout)).click();;
        }
        */
    }

}