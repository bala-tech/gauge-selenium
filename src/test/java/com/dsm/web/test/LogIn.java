package com.dsm.web.test;

import com.dsm.web.test.utils.BasePage;
import com.dsm.web.test.utils.Path;
import com.thoughtworks.gauge.Step;
import org.openqa.selenium.By;

public class LogIn extends BasePage implements Path {

    @Step("Give an option to Log In")
    public void giveAnOptionToLogIn() {
        waitForXPathElement(lnk_login);

        driver.findElement(By.xpath(lnk_login)).click();

        //waitForCSSElement(user_name);
        //driver.findElement(By.cssSelector(menuLogin)).click();
    }

    @Step("Login as name <name> and <password>")
    public void LoginAsCustomerDetails(String name, String password) {
        waitForXPathElement(txt_username);

        driver.findElement(By.xpath(txt_username)).sendKeys(name);
        driver.findElement(By.xpath(btn_continue)).click();
        waitForXPathElement(txt_password);
        driver.findElement(By.xpath(txt_password)).sendKeys(password);
        driver.findElement(By.xpath(btn_login)).click();

        waitForXPathElement(lnk_user);
    }
}
