package com.dsm.web.test.utils;

import com.dsm.web.test.utils.driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    public void waitForXPathElement(String xpath) {
        waitFor(By.xpath(xpath));
    }

    public void waitForCSSElement(String cssSelector) {
        waitFor(By.cssSelector(cssSelector));
    }

    private void waitFor(By element) {
        WebDriverWait wait = new WebDriverWait(Driver.webDriver, 25);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }

}
