package com.dsm.web.test.utils;

import com.dsm.web.test.utils.driver.Driver;
import org.openqa.selenium.WebDriver;

public interface Path {

    WebDriver driver = Driver.webDriver;

    String lnk_login = "//a[contains(text(),'Log in')]";
    String txt_username = "//input[@id='username']";
    String btn_continue = "//span[contains(text(),'Continue')]";
    String txt_password = "//input[@id='password']";
    String btn_login = "//span[text()='Log in']/..";

    String lnk_user = "//button/div/span[@role='img']";
    String lnk_logout = "//span[contains(text(),'Log out')]";

//    String txtWelcomeUser = "//div/h3[contains(text(),'Welcome,')]"; //By.xpath();
//    //String txtUsername =  "div.ui-header.ui-bar-a div h3";
//    String menu = "//a[@id='button1']/div[contains(@style,'sprite-icons.png')]";
//    String menuLogin = "a#button1.ui-link div.menuText";
//    String menuLogout = "a#logout.ui-link div.menuText";
//
//    String inputUsername = "//input[@id='username']";
//    String inputPassword = "//input[@id='password']";
//    String btnLogin = "a#login_btn.loginPageButton.ui-link span";
}