package com.dsm.web.test.utils;

import com.thoughtworks.gauge.Step;

public class AppLauncher extends BasePage implements Path {

    public static String APP_URL = System.getenv("APP_URL");

    @Step("Load bitbucket home page")
    public void launchTheApplication() {
        driver.get(APP_URL);
        //waitForXPathElement(lnk_login);
    }
}